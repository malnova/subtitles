1
00:00:08,120 --> 00:00:11,840
LE PETIT COPAIN DE BETTY

2
00:00:29,320 --> 00:00:33,400
J'ai pas besoin de soleil
quand je suis triste.

3
00:00:34,400 --> 00:00:37,680
Tout ce que je veux,
c'est qu'on soit deux.

4
00:00:39,400 --> 00:00:44,240
Quand tu es près de moi,
je n'ai plus froid.

5
00:00:44,480 --> 00:00:49,160
Personne ne me fait
de l'effet comme toi.

6
00:00:49,440 --> 00:00:54,120
Tu gardes une place
dans mon cœur...

7
00:00:54,360 --> 00:00:59,120
Si tu parlais, quel malheur !

8
00:00:59,360 --> 00:01:03,840
Je sais que tu es
mon meilleur ami...

9
00:01:04,400 --> 00:01:09,040
Fidèle à la vie.

10
00:01:09,280 --> 00:01:14,200
Mon p'tit copain,
ne t'en va jamais...

11
00:01:14,480 --> 00:01:19,200
Vivons tous deux
dans le bonheur parfait.

12
00:01:19,440 --> 00:01:24,240
Nous resterons ensemble toujours,

13
00:01:24,480 --> 00:01:28,880
mon p'tit copain d'amour.

14
00:01:40,160 --> 00:01:42,440
Lâche ça !

15
00:01:45,680 --> 00:01:48,400
Mon dieu, mon dieu !

16
00:02:25,120 --> 00:02:28,360
Vilain chien,
regarde ce que tu as fait.

17
00:02:34,280 --> 00:02:37,960
Rentre à la maison.

18
00:02:41,800 --> 00:02:43,320
À la maison !

19
00:02:45,520 --> 00:02:48,240
Tout de suite.
À la maison.

20
00:02:59,280 --> 00:03:01,080
<i>Fourrière</i>

21
00:03:13,920 --> 00:03:17,240
Non, c'est mon chien à moi !

22
00:03:29,040 --> 00:03:33,840
Pourquoi m'as-tu donc
brisé le cœur...

23
00:03:34,080 --> 00:03:38,800
Pour moi, maintenant,
c'est le malheur.

24
00:03:39,040 --> 00:03:43,880
Je sais que j'ai perdu
mon meilleur ami...

25
00:03:44,160 --> 00:03:48,320
Fidèle à la vie...

26
00:04:01,080 --> 00:04:03,520
Reviens, reviens !

27
00:04:04,280 --> 00:04:08,320
S'il te plaît,
ne t'en va plus jamais...

28
00:04:24,640 --> 00:04:27,000
Mon pauvre petit chien !

29
00:04:34,720 --> 00:04:36,120
Prends ça !

30
00:05:58,960 --> 00:06:02,840
Tu es revenu !

31
00:06:03,760 --> 00:06:08,200
Je t'en prie, ne me quitte jamais...

32
00:06:08,440 --> 00:06:13,120
Dis-moi que nous
resterons toujours ensemble...

33
00:06:13,400 --> 00:06:17,960
Nous resterons ensemble
pour la vie,

34
00:06:18,240 --> 00:06:22,160
mon petit chien chéri.

