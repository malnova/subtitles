1
00:00:08,200 --> 00:00:11,000
BETTY, UN PRIX DE CHOIX

2
00:00:29,960 --> 00:00:33,240
THÉÂTRE DE DODOVILLE

3
00:00:38,240 --> 00:00:40,240
LE TRIOMPHE DE LA VERTU

4
00:00:40,480 --> 00:00:43,320
avec Betty Boop
et Fred le Costaud

5
00:00:43,560 --> 00:00:45,240
Casting d'enfer !

6
00:01:01,960 --> 00:01:03,720
Salut tout le monde !

7
00:01:04,760 --> 00:01:07,360
Je vais vous montrer ce qu'on
peut faire avec un pistolet.

8
00:01:11,400 --> 00:01:13,400
Ça vous la coupe, hein ?

9
00:01:13,800 --> 00:01:15,040
ÉCOLE MUNICIPALE

10
00:01:21,160 --> 00:01:22,800
Cher public !

11
00:01:23,120 --> 00:01:25,480
ATTENTION ! PHILLIP-LE-DÉMON
RODE DANS LES PARAGES

12
00:01:25,760 --> 00:01:27,400
Oh là là !

13
00:01:32,240 --> 00:01:34,160
Rappelez-vous !
En cas de coup dur...

14
00:01:34,400 --> 00:01:38,360
si vous avez besoin
d'un shériff, je suis là !

15
00:01:38,760 --> 00:01:41,480
Vous êtes très aimable !

16
00:01:41,720 --> 00:01:43,000
Et mignon, en plus !

17
00:02:10,960 --> 00:02:12,840
Alors comme ça,
ils me cherchent ?

18
00:02:18,280 --> 00:02:22,720
Quelle beauté !
Bientôt, elle sera à moi.

19
00:02:43,600 --> 00:02:44,480
Attention !

20
00:02:46,960 --> 00:02:48,640
Pas si fort !

21
00:02:54,160 --> 00:02:56,160
Tu es en mon pouvoir !

22
00:02:58,280 --> 00:03:01,120
- Qu'allez-vous faire de moi ?
- Tout dépend de toi !

23
00:03:01,360 --> 00:03:03,360
Tu m'appartiens !

24
00:03:03,920 --> 00:03:06,200
Tu es en mon pouvoir !

25
00:03:08,320 --> 00:03:11,120
- C'est mon argent que vous voulez ?
- C'est TOI, beauté !

26
00:03:11,360 --> 00:03:13,080
Et jamais je ne te lâcherai !

27
00:03:13,360 --> 00:03:16,320
<i>Comment osez-vous ?</i>

28
00:03:16,560 --> 00:03:18,520
<i>Lâchez-moi !</i>

29
00:03:18,760 --> 00:03:23,400
<i>Quel sale type !
Je vous corrigerais si je le pouvais.</i>

30
00:03:24,040 --> 00:03:26,400
<i>Mais tu es en mon pouvoir !</i>

31
00:03:28,400 --> 00:03:33,120
<i>Comme méchant, je suis le meilleur.
Alors, cherche pas... Tu es à moi !</i>

32
00:03:33,360 --> 00:03:36,200
<i>Tu es en mon pouvoir !</i>

33
00:03:45,000 --> 00:03:46,560
Ah, te voilà !

34
00:03:58,480 --> 00:04:00,600
Mon fidèle coursier !

35
00:04:09,660 --> 00:04:10,520
Arrêtez !

36
00:04:10,660 --> 00:04:12,120
Je vais me le payer !

37
00:04:19,320 --> 00:04:20,680
C'est toi qui va payer !

38
00:04:35,880 --> 00:04:37,200
Au secours !

39
00:04:42,320 --> 00:04:44,000
Ah, te voilà !

40
00:05:02,120 --> 00:05:03,360
Une seconde !

41
00:05:05,000 --> 00:05:07,080
Épouse-moi... ou tu es morte !

42
00:05:07,600 --> 00:05:09,760
Je préfère vous épou...

43
00:05:10,000 --> 00:05:12,440
...euh, mourir,
je veux dire !

44
00:05:14,200 --> 00:05:15,120
Au secours !

45
00:05:20,680 --> 00:05:24,800
Courage, ma fille !
Les secours arrivent !

46
00:05:37,560 --> 00:05:38,600
Pitié !

47
00:05:41,560 --> 00:05:43,640
J'arrive !

48
00:05:52,640 --> 00:05:54,080
Juste à temps !

49
00:05:55,120 --> 00:05:56,560
Ah, te voilà !

50
00:05:57,560 --> 00:05:58,680
Encore toi ?

51
00:06:00,000 --> 00:06:01,440
Eh bien, prends ça !

52
00:06:02,480 --> 00:06:03,960
Et ça !

53
00:06:10,040 --> 00:06:12,000
Voilà pour toi !

54
00:06:23,120 --> 00:06:26,440
Par Saint Buffalo Bill !
Mais c'est Phillip-le-Démon !

55
00:06:33,760 --> 00:06:35,760
- Mon héros !
- Ma Betty !

56
00:06:40,000 --> 00:06:41,280
Merci !

57
00:06:42,200 --> 00:06:44,120
Merci à tous.

