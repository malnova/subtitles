1
00:00:07,860 --> 00:00:11,380
<i>Betty est la reine des toons !</i>

2
00:00:11,660 --> 00:00:15,340
<i>Attendez de la mater !</i>

3
00:00:15,660 --> 00:00:17,100
<i>Avec son p'tit clin d'œil...</i>

4
00:00:17,340 --> 00:00:19,220
<i>...elle vous fera craquer !</i>

5
00:00:19,540 --> 00:00:23,020
<i>Attendez de la mater !</i>

6
00:00:23,260 --> 00:00:24,500
<i>Elle a de ces yeux !</i>

7
00:00:24,780 --> 00:00:26,380
<i>Et ce petit nez !</i>

8
00:00:26,820 --> 00:00:30,540
<i>Elle a tout ce qu'il faut
où il faut !</i>

9
00:00:31,420 --> 00:00:32,980
<i>Si vous voulez vérifier...</i>

10
00:00:33,220 --> 00:00:34,980
<i>...comment qu'elle est roulée...</i>

11
00:00:35,380 --> 00:00:40,740
<i>...attendez de la mater !</i>

12
00:00:48,940 --> 00:00:52,180
En piste pour le spectacle !

13
00:00:52,420 --> 00:00:55,100
Prenez vos tickets !
Dépêchez-vous !

14
00:01:44,940 --> 00:01:47,740
Voici maintenant Miss Jeannie !

15
00:01:47,980 --> 00:01:52,500
Elle va exécuter le saut de la mort...
et retomber dans un bol de soupe !

16
00:01:52,900 --> 00:01:55,620
On l'applaudit bien fort !

17
00:02:04,700 --> 00:02:06,580
Vous êtes prête ?

18
00:02:14,260 --> 00:02:17,060
Quelqu'un m'a touché !

19
00:02:22,980 --> 00:02:26,060
Cacahuètes ! Cacahuètes !

20
00:02:31,580 --> 00:02:32,780
Cacahuètes ?

21
00:02:34,060 --> 00:02:35,020
Non !

22
00:03:06,780 --> 00:03:07,620
Cacahuètes !

23
00:03:07,860 --> 00:03:08,960
NON !

24
00:03:51,940 --> 00:03:55,820
Je rrrugis plus forrrt que toi !

25
00:04:11,500 --> 00:04:12,580
Regardez !

26
00:04:21,700 --> 00:04:25,100
Avec votre permission...
Vous avez perdu votre mouchoir.

27
00:04:25,660 --> 00:04:26,540
Merci.

28
00:04:30,020 --> 00:04:32,700
Cacahuètes ! Cacahuètes !

29
00:04:40,460 --> 00:04:41,780
Finalement,
j'en veux bien.

30
00:04:44,020 --> 00:04:45,020
Mes cacahuètes !

31
00:04:55,780 --> 00:04:59,780
Et maintenant...
Betty la funambule !

32
00:05:09,260 --> 00:05:11,940
<i>On se sent seule tout là-haut !</i>

33
00:05:12,340 --> 00:05:14,900
<i>Vous en bas, et moi en haut !</i>

34
00:05:15,140 --> 00:05:18,980
<i>Mais quand faut y aller,
faut y aller !</i>

35
00:05:19,220 --> 00:05:20,500
<i>Boop-boop-a-doop !</i>

36
00:05:23,980 --> 00:05:26,660
<i>Qui ne risque rien n'a rien !</i>

37
00:05:28,340 --> 00:05:30,140
<i>Venez me chercher !
Faites quelque chose !</i>

38
00:05:30,860 --> 00:05:32,140
<i>Boop-boop-a-doop !</i>

39
00:05:33,180 --> 00:05:36,020
<i>C'est le moment, c'est l'instant !</i>

40
00:05:37,740 --> 00:05:38,940
<i>J'ai un banc et un parc.</i>

41
00:05:39,180 --> 00:05:41,180
<i>J'ai un parc et un banc et tout...</i>

42
00:05:47,580 --> 00:05:49,940
<i>Dépêchez, je fatigue !</i>

43
00:05:53,020 --> 00:05:56,020
<i>Venez me boop-boop-a-dooper !</i>

44
00:06:02,420 --> 00:06:04,340
Je vais attendre qu'elle sorte.

45
00:06:24,060 --> 00:06:26,100
Te voilà enfin, beauté !

46
00:06:30,380 --> 00:06:31,500
Arrêtez !

47
00:06:32,140 --> 00:06:34,020
T'aimes pas ton travail ?

48
00:06:36,420 --> 00:06:39,420
Bon alors, je serais toi, je...

49
00:06:40,340 --> 00:06:41,460
Sale type !

50
00:06:43,580 --> 00:06:46,360
Tu peux dire adieu
à ton boop-boop-a-doop !

51
00:06:49,780 --> 00:06:52,420
<i>Collez-moi au pain sec et à l'eau !</i>

52
00:06:52,820 --> 00:06:55,220
<i>Faites-moi trimer sans me payer !</i>

53
00:06:56,180 --> 00:06:59,500
<i>Mais laissez-moi
mon boop-boop-a-doop.</i>

54
00:07:01,100 --> 00:07:04,700
<i>Ma vie est p'têt' infernale...</i>

55
00:07:04,940 --> 00:07:06,340
<i>J'ai p'têt' pas
l'esprit qu'il faut...</i>

56
00:07:06,620 --> 00:07:10,540
<i>Mais pitié ! M'enlevez pas
mon boop-boop-a-doop !</i>

57
00:07:13,580 --> 00:07:15,700
Laissez-moi ! Au secours !

58
00:07:17,140 --> 00:07:18,340
Je viens te sauver !

59
00:07:40,620 --> 00:07:41,540
Prends ça ! Et ça !

60
00:08:05,860 --> 00:08:08,580
Alors ? Est-ce qu'il t'a...

61
00:08:09,060 --> 00:08:10,020
Non !

62
00:08:10,580 --> 00:08:14,780
<i>Il n'a pas réussi à m'enlever
mon boop-boop-a-doop !</i>

