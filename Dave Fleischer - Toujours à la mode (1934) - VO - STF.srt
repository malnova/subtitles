1
00:00:09,650 --> 00:00:13,330
TOUJOURS À LA MODE

2
00:00:38,650 --> 00:00:40,610
<i>Exposition Betty Boop</i>

3
00:00:40,850 --> 00:00:42,770
<i>Une semaine seulement</i>

4
00:00:43,010 --> 00:00:46,450
<i>Du 31 mai au 31 juillet</i>

5
00:00:47,490 --> 00:00:48,770
Pardon.

6
00:00:49,050 --> 00:00:50,210
Je vous en prie.

7
00:00:51,050 --> 00:00:52,810
- Pardon.
- Je vous en prie.

8
00:01:06,850 --> 00:01:11,530
En cette ère de création,

9
00:01:11,810 --> 00:01:16,130
il y a des révélations,

10
00:01:16,690 --> 00:01:21,810
le téléphone et la télévision,

11
00:01:22,010 --> 00:01:25,810
ont eu leur jour d'indécision.

12
00:01:26,410 --> 00:01:31,090
Quand a volé le premier avion,

13
00:01:31,410 --> 00:01:36,490
il y eut de l'incompréhension.

14
00:01:36,770 --> 00:01:41,490
Les inventions sont très commodes.

15
00:01:41,770 --> 00:01:46,170
Nous devons rester à la mode.

16
00:01:48,410 --> 00:01:50,970
<i>Autos du futur</i>

17
00:01:53,250 --> 00:01:56,090
<i>Petite voiture
pour grande famille</i>

18
00:02:04,330 --> 00:02:06,890
<i>Voiture extrêmement profilée</i>

19
00:02:11,690 --> 00:02:15,250
<i>Cabriolet familial</i>

20
00:02:15,490 --> 00:02:16,290
<i>Cousin au 1er degré</i>

21
00:02:16,530 --> 00:02:17,290
<i>Cousin au 2e degré</i>

22
00:02:17,530 --> 00:02:18,410
<i>Cousin au 3e degré</i>

23
00:02:18,610 --> 00:02:20,450
<i>Belle-mère</i>

24
00:02:21,970 --> 00:02:24,330
<i>Cabriolet privatif</i>

25
00:02:30,450 --> 00:02:32,090
<i>Roues libres</i>

26
00:02:37,330 --> 00:02:39,170
<i>La tarinola six cylindres</i>

27
00:03:01,530 --> 00:03:03,970
<i>Articles ménagers</i>

28
00:03:06,250 --> 00:03:08,770
<i>Landau pour qintuplés</i>

29
00:03:14,290 --> 00:03:18,290
<i>Demi-queue convertible</i>

30
00:03:41,850 --> 00:03:42,930
<i>Entracte</i>

31
00:03:49,330 --> 00:03:54,130
Ça ne vous coûtera pas très cher,

32
00:03:54,410 --> 00:03:59,010
Si vous voulez, mon cher,
être à la mode.

33
00:03:59,250 --> 00:04:04,210
Tout dépend des accessoires,

34
00:04:04,490 --> 00:04:08,610
de petits riens, car il faut voir
à rester à la mode.

35
00:04:09,010 --> 00:04:13,850
Ne restez pas comme 2 ronds de flan,

36
00:04:14,090 --> 00:04:18,650
soyez plus intelligents.

37
00:04:19,370 --> 00:04:24,090
Faites bonne impression
grâce à vos intuitions,

38
00:04:24,370 --> 00:04:28,690
et vous inventerez la mode.

39
00:04:37,370 --> 00:04:41,130
Vous serez toujours à la mode.

40
00:04:43,130 --> 00:04:46,050
C'est chou, qu'en pensez-vous ?

41
00:04:50,490 --> 00:04:53,250
<i>Betty Boop crée une nouvelle mode</i>

42
00:04:54,850 --> 00:04:57,290
<i>Paris à l'heure Betty</i>

43
00:04:57,570 --> 00:05:00,090
<i>Une sensation s'empare de la nation</i>

44
00:05:01,410 --> 00:05:06,530
<i>Vague de fond dans la mode</i>

45
00:05:44,130 --> 00:05:48,970
Pas besoin d'être millionnaire,

46
00:05:49,210 --> 00:05:54,130
pour faire preuve d'un peu de flair.

47
00:05:54,530 --> 00:05:59,250
Mettez ça là et ceci ici,

48
00:05:59,490 --> 00:06:04,890
et vous serez toujours à la mode !
Boop-boop-a-doop !

