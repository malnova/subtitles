1
00:00:12,100 --> 00:00:24,420
BETTY BOOP LA VAHINÉ

2
00:01:21,740 --> 00:01:23,260
Avec son petit clin d'œil...

3
00:01:23,500 --> 00:01:25,620
...elle vous fera craquer !

4
00:01:25,940 --> 00:01:27,180
Elle est mignonne, hein ?

5
00:01:28,340 --> 00:01:30,460
La p'tite Betty.

6
00:02:34,780 --> 00:02:36,460
Quelle surprise !

7
00:02:43,980 --> 00:02:46,460
- Comment tu t'appelles ?
- Mon nom, c'est Bimbo.

8
00:02:46,700 --> 00:02:48,140
Tu sais chanter, Bimbo ?

9
00:02:48,380 --> 00:02:52,820
Oui, si tu m'accompagnes.

10
00:02:51,620 --> 00:02:55,140
<i>Belle Olinoha</i>

11
00:02:56,060 --> 00:02:59,900
<i>perle d'Aluha.</i>

12
00:03:00,420 --> 00:03:03,980
<i>Tes yeux noirs pleins de rêve...</i>

13
00:03:04,660 --> 00:03:07,780
<i>...promettent le paradis.</i>

14
00:03:08,620 --> 00:03:11,540
<i>Mon ukulele...</i>

15
00:03:12,500 --> 00:03:15,660
<i>...j'en jouerai toute la journée...</i>

16
00:03:16,580 --> 00:03:19,660
<i>...dans la brise embaumée...</i>

17
00:03:20,140 --> 00:03:22,660
<i>...en caressant mon rêve.</i>

18
00:03:46,740 --> 00:03:48,740
N'aie pas peur, Bimbo.

19
00:05:07,980 --> 00:05:10,020
Ah, toi chasseur !

20
00:05:10,300 --> 00:05:12,140
Je les ai possédés !

21
00:05:53,300 --> 00:05:54,220
Musique !

22
00:06:20,860 --> 00:06:23,420
Vous n'avez encore rien vu !
Regardez-moi !

23
00:07:39,940 --> 00:07:41,420
Dépêche-toi, Bimbo !

24
00:07:54,940 --> 00:07:56,380
Et si on nous voyait ?

25
00:07:56,620 --> 00:07:57,940
J'ai tout prévu !

