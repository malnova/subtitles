1
00:00:18,560 --> 00:00:22,000
DOCTEUR BETTY BOOP

2
00:00:34,440 --> 00:00:38,360
Avec son petit clin d'œeil
elle vous fera craquer

3
00:00:38,760 --> 00:00:40,600
Elle est mignonne, hein ?

4
00:00:41,080 --> 00:00:43,160
La p'tite Betty !

5
00:00:54,960 --> 00:00:56,920
<i>Achetez Jippo</i>

6
00:00:57,200 --> 00:01:00,480
<i>Aplatit les pieds,
vieillit les jeunes,</i>

7
00:01:00,720 --> 00:01:04,120
<i>...fait tomber les dents</i>

8
00:01:04,360 --> 00:01:07,640
<i>Achetez JIPPO !</i>

9
00:01:15,080 --> 00:01:17,240
Mesdames et messieurs.

10
00:01:17,920 --> 00:01:22,160
Nous avons le plaisir d'...

11
00:01:33,080 --> 00:01:35,040
... le grand Koko !

12
00:02:04,160 --> 00:02:06,040
Personne ne veut de Jippo ?

13
00:02:22,480 --> 00:02:25,480
Betty, à toi !

14
00:02:26,680 --> 00:02:28,160
Bonjour tout le monde !

15
00:02:31,840 --> 00:02:36,240
Si vous êtes riche et bien portant...

16
00:02:36,520 --> 00:02:39,480
Achetez du Jippo maintenant,
Si vous vous portez normalement.

17
00:02:40,600 --> 00:02:45,000
Il y a une grande opération
pour soigner la nation.

18
00:02:45,280 --> 00:02:48,240
Mais il faut régler la question

19
00:02:49,160 --> 00:02:50,800
Du prix de la consultation.

20
00:02:51,040 --> 00:02:53,080
Pour une somme très modique...

21
00:02:53,360 --> 00:02:56,920
...nous vous enlevons l'appendicite.

22
00:02:57,400 --> 00:02:59,720
Et s'il vous manque un organe,

23
00:03:00,080 --> 00:03:02,160
On vous fera payer le même prix.

24
00:03:02,400 --> 00:03:05,520
Achetez Jippo dès aujourd'hui !

25
00:03:07,400 --> 00:03:08,720
C'est ici qu'il faut commencer...

26
00:03:08,960 --> 00:03:12,800
...la théorie pour opérer.

27
00:03:13,040 --> 00:03:17,400
Mais si vous la jouez pathétique,
ça vous rendra tout lymphatique.

28
00:03:20,560 --> 00:03:21,800
C'est pas tout ça !

29
00:03:24,600 --> 00:03:26,280
Venez les gogos !

30
00:03:26,520 --> 00:03:28,880
Achetez une bouteille de Jippo.

31
00:03:44,080 --> 00:03:46,120
Très bien, mon garçon.

32
00:03:51,120 --> 00:03:53,480
Viens là, grand-père !

33
00:03:54,280 --> 00:03:56,760
J'en veux bien une bouteille.

34
00:04:00,240 --> 00:04:04,960
Je me fais vieux.

35
00:04:08,800 --> 00:04:12,360
J'ai juste ce qu'il te faut.

36
00:04:12,600 --> 00:04:14,120
Essaye un peu ça !

37
00:04:37,800 --> 00:04:39,320
D'accord,
je m'en mets quelques gouttes...

38
00:04:39,560 --> 00:04:42,360
...sur la tête,
et mes cheveux font la fête.

39
00:04:42,720 --> 00:04:45,640
J'en verse un peu et je masse fort...

40
00:04:45,400 --> 00:04:49,400
Et je compte un, deux, trois,
c'est trop fort !

41
00:05:10,840 --> 00:05:13,720
Tu n'as plus de petit ami.

42
00:05:14,320 --> 00:05:17,120
Je serai ton meilleur copain.

43
00:05:17,880 --> 00:05:20,840
Je t'achèterai de belles robes.

44
00:05:21,320 --> 00:05:24,160
T'en mettras plein la vue aux voisins.

45
00:05:24,400 --> 00:05:27,000
Quand tu marches dans la rue.

46
00:05:27,840 --> 00:05:30,400
Tu m'en mets vraiment plein la vue.

47
00:05:31,280 --> 00:05:34,360
Tes yeux sont peints,
tes cheveux sont teints.

48
00:05:34,920 --> 00:05:37,600
Que t'es belle mon poussin.

49
00:05:37,880 --> 00:05:40,440
Mais c'est pas bien...

50
00:05:40,920 --> 00:05:43,560
...que tu n'aies pas de petit copain.

