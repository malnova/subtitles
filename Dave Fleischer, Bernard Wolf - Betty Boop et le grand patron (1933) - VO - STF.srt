1
00:00:04,680 --> 00:00:06,000
Betty !

2
00:00:06,240 --> 00:00:09,040
Avec ton p'tit
boop-boop-a-doop...

3
00:00:09,280 --> 00:00:11,280
Tu as conquis mon cœur.

4
00:00:12,840 --> 00:00:14,640
Tu as frappé très fort.

5
00:00:14,880 --> 00:00:17,520
Betty, faut qu'on s'y colle !

6
00:00:18,560 --> 00:00:21,520
BETTY BOOP
ET LE GRAND PATRON

7
00:00:21,760 --> 00:00:25,080
Préparons-nous pour le mariage.

8
00:00:25,320 --> 00:00:29,080
Et si tu nous
boop-boop-a-doopais...

9
00:00:29,320 --> 00:00:31,840
un p'tit truc vite fait ?

10
00:00:32,760 --> 00:00:36,760
Avec son p'tit clin d'œil,
elle vous fera craquer...

11
00:00:37,040 --> 00:00:38,520
Elle est mignonne, hein ?

12
00:00:39,280 --> 00:00:41,920
La petite Betty !

13
00:00:53,760 --> 00:00:57,820
EMPLOYÉE DEMANDÉE
BIEN ROULÉE DE PRÉFÉRENCE

14
00:01:13,960 --> 00:01:15,200
Pas mal !

15
00:01:15,440 --> 00:01:17,200
Qu'est-ce que tu sais faire ?

16
00:01:18,840 --> 00:01:23,280
En société, je fais pas grand effet.
Mais en tête à tête...

17
00:01:23,920 --> 00:01:26,560
je vous surprendrai.

18
00:01:26,800 --> 00:01:28,600
Comme dactylo,
je suis pas costaud...

19
00:01:28,840 --> 00:01:31,720
Mais emmenez-moi chez vous...

20
00:01:32,080 --> 00:01:34,240
et je vous surprendrai !

21
00:01:34,760 --> 00:01:39,000
Ça me ferait de la peine
que vous en preniez une autre.

22
00:01:39,240 --> 00:01:42,720
Ne vous fiez pas aux apparences.

23
00:01:43,800 --> 00:01:46,440
J'ai un joli minois, mais...

24
00:01:55,640 --> 00:01:58,160
je vous surprendrai !

25
00:01:58,400 --> 00:02:02,760
Je ne prends pas trop de place.
Mais sur vos genoux...

26
00:02:03,480 --> 00:02:06,240
je vous surprendrai !

27
00:02:06,960 --> 00:02:10,240
Si c'est une standardiste
que vous cherchez...

28
00:02:10,480 --> 00:02:14,880
je suis peut-être pas la perle

29
00:02:15,120 --> 00:02:19,880
mais pour la conversation...
je vous surprendrai !

30
00:02:20,360 --> 00:02:21,800
Je vous engage !

31
00:02:52,960 --> 00:02:56,400
Allô ! Pourriez-vous
me rendre un petit service ?

32
00:03:26,120 --> 00:03:28,600
Je t'ai fait mal,
mon gros lapin ?

33
00:03:28,840 --> 00:03:32,320
Ça va, Betty.
Et si tu m'embrassais ?

34
00:03:33,840 --> 00:03:35,600
Quel coquin !

35
00:03:49,560 --> 00:03:50,960
Au secours ! Police !

36
00:03:54,680 --> 00:03:57,800
PEAU LISSE
Ne pas déranger

37
00:04:02,440 --> 00:04:05,400
Allô ! Voitures 4, 3, 2, 1 !

38
00:04:08,320 --> 00:04:09,480
Changement !

39
00:04:10,400 --> 00:04:14,440
Foncez au carrefour de la 4e
et de la Grande rue !

40
00:04:25,520 --> 00:04:27,800
Répondez, quelqu'un !

41
00:05:04,280 --> 00:05:07,560
Grouillez-vous !
Faut choper ce gars !

42
00:05:07,840 --> 00:05:09,040
En voilà un  !

43
00:05:12,880 --> 00:05:13,960
On y va !

44
00:05:38,080 --> 00:05:40,400
Je veux jouer aussi !

45
00:05:54,480 --> 00:05:55,920
Abattez-le, les gars !

46
00:06:15,400 --> 00:06:16,400
Malotrus !

47
00:06:16,640 --> 00:06:19,280
Qu'est-ce que vous dites de ça ?

