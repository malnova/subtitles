1
00:00:13,080 --> 00:00:18,680
BETTY BOOP AUX FOURNEAUX

2
00:00:31,000 --> 00:00:32,400
Avec son p'tit clin d'œeil...

3
00:00:32,680 --> 00:00:34,600
...elle vous fera craquer !

4
00:00:34,880 --> 00:00:35,960
Elle est mignonne, hein ?

5
00:00:37,000 --> 00:00:39,040
La p'tite Betty !

6
00:00:39,720 --> 00:00:42,840
CASSE-CROUTE

7
00:01:14,800 --> 00:01:16,640
On attrape sa partenaire...

8
00:01:18,520 --> 00:01:19,960
Et on tourne à droite !

9
00:01:36,760 --> 00:01:38,360
Pour vous... cette rose !

10
00:01:58,760 --> 00:02:00,000
Je l'ai !

11
00:02:07,480 --> 00:02:08,720
Qu'est-ce que vous prendrez ?

12
00:02:09,560 --> 00:02:11,000
Ce que je prendrai ?

13
00:02:11,280 --> 00:02:13,640
Attendez voir...

14
00:02:14,080 --> 00:02:17,160
CRÊPES, CRÊPES ET CRÊPES

15
00:02:18,520 --> 00:02:19,720
Vous n'avez pas de crêpes ?

16
00:02:20,000 --> 00:02:22,560
Non, mais j'ai
de très bonnes crêpes.

17
00:02:23,960 --> 00:02:26,200
Je prendrai juste
quelques crêpes, alors...

18
00:02:26,600 --> 00:02:27,560
Ça marche !

19
00:02:28,200 --> 00:02:29,040
Et voilà !

20
00:02:39,240 --> 00:02:40,440
Plus vite !

21
00:02:41,360 --> 00:02:42,880
Vous n'aimez pas mes crêpes ?

22
00:02:50,400 --> 00:02:52,280
Un bol de soupe bien chaude !

23
00:02:58,760 --> 00:03:00,200
Mais elle est pas chaude !

24
00:03:08,080 --> 00:03:09,480
Oh, désolée !

25
00:03:11,200 --> 00:03:12,520
Belle vue !

26
00:03:15,560 --> 00:03:17,360
Salut, Bill.

27
00:03:20,120 --> 00:03:22,280
Ma main ! Ouh !

28
00:03:24,640 --> 00:03:26,160
Passez le sucre.

29
00:03:26,400 --> 00:03:28,680
Passe le sel.
Passe un œuf.

30
00:03:29,040 --> 00:03:31,520
Et pour moi, du poulet !
Passe une cuisse.

31
00:03:31,760 --> 00:03:34,600
Tu passe ce couteau
ou faut que je supplie à genoux ?

32
00:03:35,000 --> 00:03:36,720
Passez le sucre.

33
00:03:36,960 --> 00:03:39,400
Passez la sauce.
Passe le rôti.

34
00:03:39,640 --> 00:03:42,200
Passe la crème !
Passe les toasts !

35
00:03:42,440 --> 00:03:44,880
Passe le maïs,
et puis ce que je préfère !

36
00:03:45,120 --> 00:03:47,400
Passez le sucre.

37
00:03:47,640 --> 00:03:50,120
Passe les fruits !
Passe le poisson !

38
00:03:50,360 --> 00:03:52,520
Envoie l'assiette anglaise,
elle a l'air excellente.

39
00:03:52,760 --> 00:03:54,960
Passe les noix !
Tu vois ce que je veux dire !

40
00:03:58,040 --> 00:04:00,760
Dites-moi
ce que vous prendrez...

41
00:04:01,000 --> 00:04:03,480
...et arrêtez de jacasser !

42
00:04:03,760 --> 00:04:06,160
Passe le fromage, les crackers
et les petits pois !

43
00:04:06,400 --> 00:04:08,520
PASSEZ LE SUCRE !

44
00:05:01,720 --> 00:05:03,240
Des crêpes !

45
00:05:05,280 --> 00:05:06,920
Des crêpes qui marchent !

46
00:05:25,560 --> 00:05:26,600
Encore !

47
00:05:39,520 --> 00:05:40,640
La paix !

48
00:05:43,160 --> 00:05:44,000
Encore !

