1
00:00:14,680 --> 00:00:18,000
LE MUSÉE DE BETTY

2
00:00:47,160 --> 00:00:50,640
En voiture pour le musée !

3
00:00:50,920 --> 00:00:53,080
Par ici...

4
00:00:53,360 --> 00:00:56,000
On se dépêche...

5
00:01:05,120 --> 00:01:07,520
Par ici, la petite dame...

6
00:01:15,800 --> 00:01:18,920
En voiture.
Plus qu'une place de libre.

7
00:01:21,600 --> 00:01:22,800
Plus qu'une place de libre ?

8
00:01:27,040 --> 00:01:30,080
C'est ça ?

9
00:01:47,200 --> 00:01:49,000
Je ne veux pas m'arrêter !

10
00:01:54,520 --> 00:01:56,000
<i>MUSÉE</i>

11
00:01:57,480 --> 00:01:59,040
On y est !

12
00:02:00,240 --> 00:02:01,680
Par ici, Madame.

13
00:02:02,920 --> 00:02:03,880
Merci !

14
00:02:12,640 --> 00:02:13,880
Bon dieu !

15
00:02:18,680 --> 00:02:20,280
Super la nana !

16
00:02:24,080 --> 00:02:26,000
Puis-je quitter la salle ?

17
00:02:26,280 --> 00:02:27,160
D'accord.

18
00:02:39,960 --> 00:02:41,760
<i>Hercule et le lion</i>

19
00:02:48,760 --> 00:02:51,760
Tes meilleurs amis
ne te le diraient pas.

20
00:03:01,080 --> 00:03:02,080
<i>La faim</i>

21
00:03:02,360 --> 00:03:04,400
Le pauvre homme !

22
00:03:16,880 --> 00:03:18,000
Tout le monde dehors !

23
00:03:19,000 --> 00:03:20,800
J'ai dit tout le monde !

24
00:03:21,080 --> 00:03:22,560
Attendez-moi !

25
00:03:24,960 --> 00:03:26,080
Bonsoir.

26
00:03:37,240 --> 00:03:38,520
Sortez d'ici !

27
00:03:40,800 --> 00:03:44,280
Je retourne dormir 1000 ans.

28
00:03:52,640 --> 00:03:53,960
On ferme !

29
00:03:56,200 --> 00:03:58,320
On dirait que tu n'as plus faim.

30
00:04:03,200 --> 00:04:04,720
Je suis toute seule !

31
00:04:11,160 --> 00:04:13,600
Laissez-moi sortir !

32
00:04:21,840 --> 00:04:23,680
Il fait si sombre.

33
00:05:20,040 --> 00:05:22,480
- Chante pour nous.
- Je ne sais pas !

34
00:05:22,760 --> 00:05:23,760
Chante !

35
00:05:24,240 --> 00:05:27,720
Jamais je n'aurais pensé
qu'un humain...

36
00:05:28,000 --> 00:05:31,240
...pouvait être si inhumain.

37
00:05:31,640 --> 00:05:34,880
Elle m'a kidnappé avec mon ami.

38
00:05:35,160 --> 00:05:37,360
Est-ce humain de faire ceci ?

39
00:05:38,440 --> 00:05:41,520
J'ai beau essayer
d'arranger les choses,

40
00:05:41,800 --> 00:05:44,120
ce qui doit arriver, va arriver.

41
00:05:44,400 --> 00:05:49,680
Tu m'as vraiment maltraitée.

42
00:05:50,840 --> 00:05:54,320
Tu m'as mise
derrière une porte close.

43
00:05:54,600 --> 00:05:57,880
Et je ne peux m'échapper.

44
00:05:58,360 --> 00:06:01,520
Peut-on vraiment y échapper ?

45
00:06:01,800 --> 00:06:04,920
Est-ce humain de faire ceci ?

46
00:06:25,760 --> 00:06:27,200
Au secours ! Je vous en supplie.

