1
00:00:14,960 --> 00:00:18,040
DES HAUTS ET DES BAS

2
00:00:33,880 --> 00:00:37,760
Avec son p'tit clin d'œil,
elle vous fera craquer...

3
00:00:38,200 --> 00:00:39,640
Elle est mignonne, hein ?

4
00:00:40,680 --> 00:00:42,880
La p'tite Betty !

5
00:00:58,560 --> 00:01:01,520
Je n'ai même pas un tout petit toit

6
00:01:01,760 --> 00:01:04,840
sous lequel je me sente chez moi.

7
00:01:05,800 --> 00:01:08,840
J'enlève le haut et j'ôte mes bas,

8
00:01:09,080 --> 00:01:12,280
mais je ne suis jamais chez moi.

9
00:01:13,240 --> 00:01:16,840
Je ne sais pas, je ne sais plus...

10
00:01:17,080 --> 00:01:19,520
...où est ma maison, dans quelle rue.

11
00:01:20,600 --> 00:01:23,560
Car il n'est pas un lieu sur terre

12
00:01:23,840 --> 00:01:26,240
que je puisse appeler ma chaumière.

13
00:01:46,960 --> 00:01:48,000
Salut, poulette.

14
00:01:50,760 --> 00:01:52,240
Hue, Napoléon !

15
00:01:53,440 --> 00:01:54,680
<i>À vendre.</i>

16
00:02:15,720 --> 00:02:18,360
<i>Prix à débattre.</i>

17
00:02:35,960 --> 00:02:38,880
Venez vite, les amis !

18
00:02:43,000 --> 00:02:47,520
Combien me donnerez-vous
pour cette grosse boule ?

19
00:02:47,760 --> 00:02:49,720
Y'a pas moins cher, il y a la mer...
...et c'est très cool.

20
00:02:49,960 --> 00:02:52,120
J'en donne 50.

21
00:02:54,800 --> 00:02:56,800
50 ? C'est une misère !

22
00:02:57,040 --> 00:02:59,240
Allons, voyons ?

23
00:02:59,480 --> 00:03:01,400
Il n'y a pas mieux dans l'univers.

24
00:03:01,640 --> 00:03:03,960
Je mets 40.

25
00:03:06,400 --> 00:03:10,840
40 pour l'instant.

26
00:03:13,400 --> 00:03:15,640
J'en donne 20.

27
00:03:16,800 --> 00:03:18,280
20 devant moi.

28
00:03:18,880 --> 00:03:20,880
Rien ne va plus...

29
00:03:21,880 --> 00:03:22,960
C'est vendu !

30
00:03:23,200 --> 00:03:25,400
Je les ai eux. L'univers est à moi.

31
00:03:26,200 --> 00:03:28,160
- Aboule, aboule !
- Le pognon ! le pognon !

32
00:03:28,400 --> 00:03:31,000
Comptant !

33
00:03:33,120 --> 00:03:35,200
Alors... qu'est-ce que je vais faire ?

34
00:03:37,000 --> 00:03:41,000
Je vais supprimer la gravité.

35
00:05:19,160 --> 00:05:21,040
Désolée, monsieur l'oiseau.

36
00:05:21,280 --> 00:05:23,280
Surtout ne vous fâchez pas !

37
00:05:23,760 --> 00:05:26,640
Elle a couché avec les Indiens.

38
00:05:27,400 --> 00:05:30,000
On me l'avait dit.

39
00:05:32,520 --> 00:05:33,960
Oui, oui.

40
00:05:34,200 --> 00:05:36,040
Je me sens légère.

41
00:05:36,280 --> 00:05:37,640
J'ai l'impression, Mme Berg...

42
00:05:56,880 --> 00:05:59,200
...que je me suis fait avoir.

43
00:06:24,160 --> 00:06:25,560
J'enlève le haut et j'ôte mes bas...

44
00:06:30,000 --> 00:06:32,200
Il n'est pas un lieu...

45
00:06:34,000 --> 00:06:36,280
...sur cette terre...

46
00:06:43,040 --> 00:06:45,920
...que je puisse appeler
ma chaumière.

