1
00:00:09,560 --> 00:00:12,880
RESTAURANT EN FOLIE

2
00:00:55,400 --> 00:00:56,320
Un canard rôti !

3
00:01:18,880 --> 00:01:20,800
Un sauté de bœuf.

4
00:01:21,640 --> 00:01:23,440
Et deux sautés, deux !

5
00:01:25,160 --> 00:01:27,800
Voudrais-tu
me hacher ce steak menu ?

6
00:01:32,000 --> 00:01:34,240
Une part de crème au lait.

7
00:01:34,920 --> 00:01:36,560
Avec de la moutarde.

8
00:01:36,800 --> 00:01:40,000
Change-moi ces patates
en un plat de tomates et...

9
00:01:51,480 --> 00:01:52,720
Un jambon rôti.

10
00:01:53,880 --> 00:01:55,240
<i>Che feux un chambon.</i>

11
00:01:56,160 --> 00:01:59,200
Un œuf au bacon et en vitesse !
<i>Schnell !</i>

12
00:02:22,880 --> 00:02:35,760
Où est mon canard rôti ?

13
00:02:33,560 --> 00:02:35,400
Ça vient, Monsieur...

14
00:02:35,640 --> 00:02:37,360
...tout de suite !

15
00:02:37,840 --> 00:02:41,080
Un canard rôti avec sa sauce...

16
00:02:41,320 --> 00:02:43,400
bien grasse !

17
00:02:46,720 --> 00:02:48,160
De l'amour,

18
00:02:48,760 --> 00:02:51,240
je veux de l'amour,

19
00:02:51,480 --> 00:02:55,120
mais quand je veux de l'amour

20
00:02:55,840 --> 00:02:59,040
je dois faire Boop-boop-a-doop...

21
00:03:00,720 --> 00:03:02,320
De l'amour,

22
00:03:02,880 --> 00:03:05,400
je veux de l'amour,

23
00:03:05,640 --> 00:03:09,160
mais pour l'amour,

24
00:03:10,040 --> 00:03:12,280
c'est toi que je veux.

25
00:03:15,000 --> 00:03:19,480
Je suis si triste,
quand je t'attends.

26
00:03:20,800 --> 00:03:23,560
Je n'en peux plus.

27
00:03:23,840 --> 00:03:27,000
Embrasse-moi !

28
00:03:31,400 --> 00:03:33,760
Je veux faire Whoopi.

29
00:03:34,520 --> 00:03:37,600
Et quand je fais Whoopi,

30
00:03:38,520 --> 00:03:40,920
je dois avoir un...

31
00:05:26,600 --> 00:05:30,760
Hé, il est où ce canard rôti ?
Si je ne l'ai pas, je perds la vie.

