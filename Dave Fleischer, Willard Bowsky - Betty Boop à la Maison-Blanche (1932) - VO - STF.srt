1
00:00:13,280 --> 00:00:19,280
BETTY BOOP
À LA MAISON BLANCHE

2
00:00:32,760 --> 00:00:36,360
<i>Votez Betty ! Votez Betty !</i>

3
00:00:38,680 --> 00:00:42,040
<i>Y en a qui ont de l'argent,
ça dépend de vos relations.</i</i>

4
00:00:42,320 --> 00:00:45,600
<i>Si vous m'élisez,
chacun aura sa part.</i>

5
00:00:46,600 --> 00:00:48,240
<i>Quand je serai présidente...</i>

6
00:00:48,480 --> 00:00:50,080
<i>Quand je serai présidente...</i>

7
00:00:50,320 --> 00:00:54,120
<i>Le fric, c'est moi
qui m'en occuperai !</i>

8
00:00:54,640 --> 00:00:58,000
<i>Votez Betty ! Votez Betty !</i>

9
00:00:58,400 --> 00:01:02,000
<i>Vous avez déjà une sœur,
et vous voulez un petit frère ?</i>

10
00:01:02,360 --> 00:01:05,560
<i>Si vos parents votent pour moi,
je verrai ce que je peux faire !</i>

11
00:01:06,000 --> 00:01:09,800
<i>Votez Betty ! Votez Betty !</i>

12
00:01:16,520 --> 00:01:17,920
<i>Qui je suis ?</i>

13
00:01:18,880 --> 00:01:20,240
<i>Qui je suis ?</i>

14
00:01:23,160 --> 00:01:25,360
<i>Je suis M. Personne !</i>

15
00:01:25,960 --> 00:01:29,960
<i>Qui va baisser les impôts ?
Personne !</i>

16
00:01:30,600 --> 00:01:32,680
<i>Qui va protéger
la propriété privée ?</i>

17
00:01:32,960 --> 00:01:34,560
<i>Personne !</i>

18
00:01:35,240 --> 00:01:37,240
<i>Qui remplit votre bac à fleurs ?</i>

19
00:01:37,520 --> 00:01:39,560
<i>Qui est là
quand vous tendez la main ?</i>

20
00:01:39,880 --> 00:01:42,000
<i>Ou quand vous vous pétez
une guibolle ?</i>

21
00:01:42,240 --> 00:01:44,120
<i>M. Personne !</i>

22
00:01:49,240 --> 00:01:51,080
<i>Qui vous nourrit
quand vous avez faim ?</i>

23
00:01:51,480 --> 00:01:53,280
<i>M. Personne !</i>

24
00:01:53,880 --> 00:01:55,960
<i>Qui s'inquiète
de ce que vous devenez ?</i>

25
00:01:56,200 --> 00:01:57,960
<i>M. Personne !</i>

26
00:01:58,480 --> 00:02:00,680
<i>Quand vous vous levez
aux aurores...</i>

27
00:02:00,920 --> 00:02:02,880
<i>...pour découvrir
que le laitier a changé ?</i>

28
00:02:03,240 --> 00:02:05,600
<i>Qui est là, quand votre femme
s'est tirée ?</i>

29
00:02:05,880 --> 00:02:07,400
<i>M. Personne !</i>

30
00:02:12,160 --> 00:02:14,080
<i>Quand je serai présidente...</i>

31
00:02:14,360 --> 00:02:16,000
<i>Quand je serai présidente...</i>

32
00:02:16,280 --> 00:02:19,960
<i>...je causerai plus à la radio !</i>

33
00:02:20,200 --> 00:02:21,920
<i>Quand je serai présidente...</i>

34
00:02:24,200 --> 00:02:27,800
<i>...vous verrez comment
que je ferai tourner la barraque !</i>

35
00:02:31,320 --> 00:02:33,560
<i>Ce qu'il faut à ce pays...</i>

36
00:02:33,800 --> 00:02:37,280
<i>...c'est plus de hi-dee-ho,
de boop-a-doop...</i>

37
00:02:37,560 --> 00:02:38,520
<i>...et de glace au chocolat !</i>

38
00:02:38,600 --> 00:02:40,960
- Nous on est pour !
- Et nous on est contre !

39
00:02:43,080 --> 00:02:45,160
<i>Tout sera gratuit !</i>

40
00:02:45,400 --> 00:02:47,480
<i>Cinoche ! Boîtes de nuit ! Jazz !</i>

41
00:02:47,760 --> 00:02:49,800
− Pour ça, on te suit !
- Et nous, on fuit !

42
00:02:50,040 --> 00:02:52,460
- Sales éléphants !
- Espèces d'ânes !

43
00:02:54,720 --> 00:02:56,880
<i>Vous avez entendu
mon programme...</i>

44
00:02:57,160 --> 00:02:58,920
<i>...je suis sûre
que vous l'appréciez.</i>

45
00:02:59,400 --> 00:03:03,640
<i>Quel beau pays on va avoir
avec tous mes aménagements !</i>

46
00:04:31,440 --> 00:04:34,520
BUREAU DE LA MÉTÉO

47
00:04:45,640 --> 00:04:48,080
Qu'est-ce que c'est ?
Oh, ma parole !

48
00:05:22,600 --> 00:05:24,560
Quelle transformation !

49
00:05:26,120 --> 00:05:29,880
<i>Quand je serai présidente...</i>

50
00:05:30,160 --> 00:05:33,920
<i>...je vous ferai un bisou
gros comme ça !</i>

51
00:05:37,840 --> 00:05:41,840
BETTY BOOP EST ÉLUE !

52
00:06:11,000 --> 00:06:13,040
Merci ! Merci !

