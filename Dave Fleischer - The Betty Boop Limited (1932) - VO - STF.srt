1
00:00:14,410 --> 00:00:18,650
BETTY BOOP MÈNE LE SHOW

2
00:00:34,250 --> 00:00:36,650
Betty, dépêche-toi !
On est en retard !

3
00:00:40,970 --> 00:00:42,770
Je t'ai gardé une place !

4
00:00:44,610 --> 00:00:46,410
J'arrive, restez habillés !

5
00:00:59,450 --> 00:01:00,890
Ne me laisse pas tomber !

6
00:01:12,690 --> 00:01:14,330
Attention au départ !

7
00:01:38,090 --> 00:01:40,090
<i>Wagon restaurant</i>

8
00:02:02,330 --> 00:02:03,810
Salut, les gars !

9
00:02:04,170 --> 00:02:06,690
Allez, encore une répétition !

10
00:02:07,210 --> 00:02:09,530
Je commence.

11
00:02:13,570 --> 00:02:16,570
Est-ce que ça te rend heureux,

12
00:02:16,810 --> 00:02:19,290
Quand je dis que tu es le mieux,

13
00:02:19,770 --> 00:02:24,290
Ou non ?

14
00:02:26,610 --> 00:02:29,810
Est-ce que t'es amoureux,

15
00:02:30,050 --> 00:02:32,690
Quand je t'embrasse

16
00:02:33,130 --> 00:02:36,410
à qui mieux-mieux,

17
00:02:36,690 --> 00:02:38,050
Ou non ?

18
00:02:39,530 --> 00:02:42,650
Tu sais qu'on va se marier,

19
00:02:42,930 --> 00:02:45,770
Qu'on va dire oui à M. le Maire.

20
00:02:46,410 --> 00:02:50,290
Et dans un an,
peut-être un enfant...

21
00:02:53,570 --> 00:02:56,490
Est-ce que tu es content,

22
00:02:56,730 --> 00:02:59,770
Quand je dis que je t'aime tant,

23
00:03:00,010 --> 00:03:02,970
T'es content, chéri...

24
00:03:05,610 --> 00:03:06,650
Ou non ?

25
00:03:09,410 --> 00:03:14,050
Chérie, je me fais vieux...

26
00:03:17,770 --> 00:03:18,930
À toi, Bimbo !

27
00:03:56,010 --> 00:04:00,730
Chérie, je me fais vieux...

28
00:04:08,610 --> 00:04:10,570
Je repasserai...

29
00:04:14,170 --> 00:04:15,970
Allez, les gars !

30
00:04:30,130 --> 00:04:32,090
Donnez tout ce que vous avez !

31
00:04:35,330 --> 00:04:39,130
Chérie, je me fais vieux...

32
00:04:42,730 --> 00:04:45,810
Allez, on s'éclate pour le final.

33
00:05:33,250 --> 00:05:36,370
Chérie, je me fais vieux...

34
00:05:52,810 --> 00:05:53,850
Je l'aurai !

