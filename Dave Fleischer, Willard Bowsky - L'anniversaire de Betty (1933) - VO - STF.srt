1
00:00:06,920 --> 00:00:10,920
L'ANNIVERSAIRE DE BETTY

2
00:00:51,280 --> 00:00:54,200
J'ai l'air et j'ai les paroles.

3
00:00:54,960 --> 00:00:57,720
J'ai répété comme une folle.

4
00:00:57,960 --> 00:01:01,320
Mais personne n'écoute ma chanson.

5
00:01:01,600 --> 00:01:04,880
Et je chante seule dans mon salon.

6
00:01:05,760 --> 00:01:09,200
Il y a tant à faire,
je ne sais plus...

7
00:01:09,480 --> 00:01:12,280
Je me sens seule et perdue.

8
00:01:12,520 --> 00:01:16,000
Personne n'écoute ma chanson.

9
00:01:16,240 --> 00:01:18,920
Et je chante seule dans la maison.

10
00:01:24,200 --> 00:01:25,800
C'est le destin.

11
00:01:30,180 --> 00:01:34,920
Est-ce que quelqu'un
voudra entendre...

12
00:01:35,280 --> 00:01:38,640
...le chant d'un cœur
solitaire et tendre.

13
00:01:38,880 --> 00:01:41,320
Et chaque jour qui passe...

14
00:01:41,880 --> 00:01:45,080
...me lasse et mon cœur se casse.

15
00:01:45,600 --> 00:01:48,680
Je chante seule dans la maison.

16
00:01:57,440 --> 00:01:59,200
Mes cheveux !

17
00:02:00,400 --> 00:02:02,280
Il y a un tel désordre !

18
00:02:12,520 --> 00:02:14,120
Bon anniversaire !

19
00:02:19,920 --> 00:02:20,700
Surprise !

20
00:02:28,120 --> 00:02:29,640
Je suis si contente !

21
00:02:33,240 --> 00:02:34,720
Bon anniversaire !

22
00:02:35,520 --> 00:02:36,320
Merci !

23
00:03:00,080 --> 00:03:01,720
Nos vœux les plus sincères !

24
00:03:07,200 --> 00:03:08,880
Tous nos vœux !

25
00:03:47,640 --> 00:03:50,560
C'est l'anniversaire de Betty !

26
00:03:56,400 --> 00:03:59,120
C'est l'anniversaire de Betty !

27
00:04:13,680 --> 00:04:16,680
C'est l'anniversaire de Betty !

28
00:04:56,440 --> 00:04:58,320
- À moi !
- À moi !

29
00:04:58,560 --> 00:05:00,120
- À moi !
- À moi !

30
00:05:02,720 --> 00:05:04,120
Arrêtez de parler de <i>moi</i> !

31
00:05:37,720 --> 00:05:39,160
<i>Petits pois</i>

32
00:05:57,000 --> 00:05:58,600
<i>Œufs</i>

33
00:06:17,480 --> 00:06:18,560
Salut, Betty !

34
00:06:18,800 --> 00:06:21,000
Salut à toi, Georgy !

